class NavBar extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("nav", {
      className: "navBar"
    }, /*#__PURE__*/React.createElement("ul", {
      className: "navMenu"
    }, /*#__PURE__*/React.createElement("li", {
      className: "navButton",
      id: "goHome"
    }, "Home"), /*#__PURE__*/React.createElement("li", {
      className: "navButton",
      id: "goMakeReservation"
    }, "Go to reservation page"), /*#__PURE__*/React.createElement("li", {
      className: "navButton",
      id: "goWaitingQueue"
    }, "Check Waiting List"))));
  }

}

class Homepage extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "homepage-banner"
    }, /*#__PURE__*/React.createElement("h1", {
      className: "welcome-message"
    }, "Welcome to Hotel California")), /*#__PURE__*/React.createElement("h3", {
      class: "about-us"
    }, " Serving authentic American food, the restaurant in Hotel California was just recently awarded the best restaurant award in the country. Don't miss this opportunity! "), /*#__PURE__*/React.createElement("br", null));
  }

}

;

class QueuePage extends React.Component {
  state = {
    popup: false
  };
  togglePop = () => {
    this.setState({
      popup: !this.state.popup
    });
  };

  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "centralised"
    }, /*#__PURE__*/React.createElement("h1", null, " Don't miss this opportunity! "), /*#__PURE__*/React.createElement("p", null, " We apologise that we have limited seats at the moment. Please fill in the form below to join the waitlist. "), /*#__PURE__*/React.createElement("p", null, " We look forward to seeing you in the restaurant! "), /*#__PURE__*/React.createElement("p", null, " Number of free slots available: 30"), /*#__PURE__*/React.createElement("button", {
      onClick: this.togglePop
    }, " Grab your seat now! "), /*#__PURE__*/React.createElement(JoinQueueForm, null), /*#__PURE__*/React.createElement("h3", null, "Want to check the queue before making up your mind?"), /*#__PURE__*/React.createElement("button", null, " Click here "));
  }

}

;

class Queue extends React.Component {
  state = {
    customers: [{
      name: 'Alan',
      seats: 1,
      phoneNumber: '91234567',
      date: "19/05/2021",
      time: "13:08:04"
    }, {
      name: 'Ben',
      seats: 2,
      phoneNumber: '97654321',
      date: "19/05/2021",
      time: "13:10:04"
    }, {
      name: 'Charlie',
      seats: 3,
      phoneNumber: '12345678',
      date: "19/05/2021",
      time: "13:20:04"
    }]
  };

  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "centralised"
    }, /*#__PURE__*/React.createElement("h1", null, "Check the current queue: "), /*#__PURE__*/React.createElement("table", {
      className: "waitingList"
    }, /*#__PURE__*/React.createElement("thead", null, /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("th", null, " Name "), /*#__PURE__*/React.createElement("th", null, " Seats Required"), /*#__PURE__*/React.createElement("th", null, " Contact Details "), /*#__PURE__*/React.createElement("th", null, " Date "), /*#__PURE__*/React.createElement("th", null, " Time "))), /*#__PURE__*/React.createElement("tbody", null, this.state.customers.map(cust => /*#__PURE__*/React.createElement(Customer, {
      key: cust.name,
      customerDetails: cust
    })))));
  }

}

;

class Customer extends React.Component {
  state = {
    details: this.props.customerDetails
  };

  render() {
    return /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, this.state.details.name), /*#__PURE__*/React.createElement("td", null, this.state.details.seats), /*#__PURE__*/React.createElement("td", null, this.state.details.phoneNumber), /*#__PURE__*/React.createElement("td", null, this.state.details.date), /*#__PURE__*/React.createElement("td", null, this.state.details.time), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("button", null, " Seat Customer ")), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("button", null, " Delete From Queue ")));
  }

}

;

class JoinQueueForm extends React.Component {
  handleClick = () => {
    this.props.toggle();
  };

  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "modal"
    }, /*#__PURE__*/React.createElement("table", {
      id: "reservationForm"
    }, /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, "Name:"), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("input", {
      type: "text",
      id: "customerName",
      class: "customerName reservationFormTable"
    })), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("div", {
      id: "errName",
      class: "errName reservationFormTable"
    }))), /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, "Number of seats required:"), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("input", {
      type: "number",
      id: "seatsRequired",
      class: "seatsRequired reservationFormTable"
    })), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("div", {
      id: "errSeats",
      class: "errSeats reservationFormTable"
    }))), /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, "Phone Number:"), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("input", {
      type: "text",
      id: "phoneNumber",
      class: "phoneNumber reservationFormTable"
    })), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("div", {
      id: "errPhone",
      class: "errPhone reservationFormTable"
    })))), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("button", null, " Submit "));
  }

}

;
ReactDOM.render( /*#__PURE__*/React.createElement(Homepage, null), document.getElementById('homepage'));
ReactDOM.render( /*#__PURE__*/React.createElement(QueuePage, null), document.getElementById('queuepage'));
ReactDOM.render( /*#__PURE__*/React.createElement(Queue, null), document.getElementById('queue'));
ReactDOM.render( /*#__PURE__*/React.createElement(NavBar, null), document.getElementById('testBar'));