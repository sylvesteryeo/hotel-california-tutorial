class NavBar extends React.Component {
    render() { 
        return (
            <React.Fragment>
                <nav className="navBar"> 
                    <ul className="navMenu">
                        <li className="navButton" id="goHome">Home</li>
                        <li className="navButton" id="goMakeReservation">Go to reservation page</li>
                        <li className="navButton" id="goWaitingQueue">Check Waiting List</li>
                    </ul>
                </nav>
            </React.Fragment>
        );
    }
}

class Homepage extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div className="homepage-banner">
                    <h1 className="welcome-message" >Welcome to Hotel California</h1>
                </div>
                <h3 class="about-us"> Serving authentic American food, the restaurant in Hotel California was just recently awarded the best restaurant award in the country. Don't miss this opportunity! </h3><br></br>
            </React.Fragment>
        );
    }
};

class QueuePage extends React.Component {
    state = {
        popup: false
    };

    togglePop = () => {
        this.setState({popup: !this.state.popup});
    };

    render() {
        return (
            <div className="centralised">
                <h1> Don't miss this opportunity! </h1>
                <p> We apologise that we have limited seats at the moment. Please fill in the form below to join the waitlist. </p>
                <p> We look forward to seeing you in the restaurant! </p>
                <p> Number of free slots available: 30</p>
                <button onClick={ this.togglePop }> Grab your seat now! </button>
                <JoinQueueForm />

                <h3>Want to check the queue before making up your mind?</h3> 
                <button> Click here </button>
                {/* {this.state.popup ? <JoinQueueForm toggle={this.togglePop} /> : null} */}
            </div>

        );
    }
};

class Queue extends React.Component {
    state = {
        customers : [
            {name:'Alan', seats:1, phoneNumber:'91234567',date:"19/05/2021",time:"13:08:04"},
            {name:'Ben', seats:2, phoneNumber:'97654321',date:"19/05/2021",time:"13:10:04"},
            {name:'Charlie', seats:3, phoneNumber:'12345678',date:"19/05/2021",time:"13:20:04"},
        ]
    };

    render() { 
        return (
            <div className="centralised">
                <h1>Check the current queue: </h1>
                <table className='waitingList'>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Seats Required</th>
                            <th> Contact Details </th>
                            <th> Date </th>
                            <th> Time </th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.customers.map(cust => 
                        <Customer key = {cust.name} customerDetails={cust} />)}
                    </tbody>
                </table>
            </div >
        );
    }
};
 

class Customer extends React.Component {
    state = {
        details: this.props.customerDetails
    };

    render() { 
        return (
            <tr>
                <td>{ this.state.details.name }</td>
                <td>{ this.state.details.seats }</td>
                <td>{ this.state.details.phoneNumber }</td>
                <td>{ this.state.details.date }</td>
                <td>{ this.state.details.time }</td>
                <td><button> Seat Customer </button></td>
                <td><button> Delete From Queue </button></td>
            </tr>

        );
    }
};

class JoinQueueForm extends React.Component {
    handleClick = () => {
        this.props.toggle();
    };

    render() { 
        return (
            <div className="modal">
                <table id="reservationForm">
                    <tr>
                        <td>Name:</td>
                        <td><input type="text" id="customerName" class="customerName reservationFormTable" /></td>
                        <td><div id="errName" class="errName reservationFormTable"></div></td>
                    </tr>
                    <tr>
                        <td>Number of seats required:</td>
                        <td><input type="number" id="seatsRequired" class="seatsRequired reservationFormTable" /></td>
                        <td><div id="errSeats" class="errSeats reservationFormTable"></div></td>
                    </tr>
                    <tr>
                        <td>Phone Number:</td>
                        <td><input type="text" id="phoneNumber" class="phoneNumber reservationFormTable" /></td>
                        <td><div id="errPhone" class="errPhone reservationFormTable"></div></td>
                    </tr>
                </table>
                <br />
                <button> Submit </button>
            </div>
           );
    }
};



ReactDOM.render(<Homepage />,document.getElementById('homepage'));
  
ReactDOM.render(<QueuePage />,document.getElementById('queuepage'));
  
ReactDOM.render(<Queue />,document.getElementById('queue'));

ReactDOM.render(<NavBar />,document.getElementById('testBar'));

